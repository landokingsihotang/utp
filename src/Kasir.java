import java.util.Scanner;

public class Kasir {
    private Meja[] daftarMeja;
    private Menu[] daftarMenu;

    public Kasir() {
        daftarMeja = new Meja[10];
        for (int i = 0; i < 10; i++) {
            daftarMeja[i] = new Meja(i + 1);
        }

        daftarMenu = new Menu[5];
        daftarMenu[0] = new Menu("Nasi Goreng", 15000);
        daftarMenu[1] = new Menu("Mi Goreng", 15000);
        daftarMenu[2] = new Menu("Capcay", 20000);
        daftarMenu[3] = new Menu("Bihun Goreng", 17000);
        daftarMenu[4] = new Menu("Ayam Koloke", 25000);
    }

    // digunakan untuk menampilkan daftar meja beserta keterangan ketersediaannya
    // menggunakan method isKosong pada class Kasir 
    public void tampilkanDaftarMeja() {
        //EDIT DISINI
        for (Meja meja : daftarMeja) {
            if (!meja.isKosong()) {
                System.out.println("Meja "+meja.getNomorMeja()+" (terisi oleh pelanggan "+meja.getPelanggan().getNama()+")");
            } else {
                System.out.println("Meja "+meja.getNomorMeja()+" (kosong)");
            }
        }
    }

    //  menambahkan pelanggan pada meja tertentu
    // jika meja kosong pelanggan dapat ditambahkan
    // jika meja terisi akan ada keterangan meja telah diisi
    public void tambahPelanggan(int nomorMeja, Pelanggan pelanggan) {
        //EDIT DISINI
        Meja meja = daftarMeja[nomorMeja-1];
        if (meja.isKosong()) {
            meja.setPelanggan(pelanggan);
        } else {
            System.out.println("meja sudah terisi");
        }

    }
    
    // menambahkan pesanan menu pada nomor meja
    //menggunakan boolean agar untuk menentukan menu yang ada 
    // jika menu tidak ada dalam daftar maka tampilkan "Menu is null" 
    public void tambahPesanan(int nomorMeja, Menu menu) {
        //EDIT DISINI
        boolean pesanan = false;
        Meja meja = daftarMeja[nomorMeja-1];
        for (Menu menus : daftarMenu) {
            if (menus == menu) {
                pesanan = true;
                break;
            }
        }
        if (pesanan) {
            meja.setMenu(menu);
        } else {
            System.out.println("Menu is null");
        }
    }

    // Menghapus pelanggan 
    //ketika pelanggan pergi ,nomor yang dinputkan oleh kasir akan otomatis menghapus meja
    // namun apabila menghapus meja yang kosong ,akan ada output " Meja belum Terisi"
    public void hapusPelanggan(int nomorMeja) {
        //EDIT DISINI
        Meja meja = daftarMeja[nomorMeja-1];
        if (meja.isKosong()) {
            System.out.println("Meja belum terisi");
        } else {
            meja.setPelanggan(null);
        }
    }


    //menghitung pesanan dengan menggunakan perulangan
    //total harga akan dihitung dengan mengambil harga dari menu (menu.getHarga)
    public int hitungHargaPesanan(int nomorMeja) {
        int totalHarga = 0;
        Meja meja = daftarMeja[nomorMeja - 1];
        Pelanggan pelanggan = meja.getPelanggan();
        Menu[] menu = meja.getMenu();
        if (pelanggan != null && menu != null && menu.length > 0) {
            for (int i = 0; i < menu.length; i++) {
                if (menu[i] != null) {
                    totalHarga += menu[i].getHarga();
                }
            }
            return totalHarga;
        }
        return totalHarga;
    }

    //menampilkan pesanan dengan melakukan perulangan sederhana
    //menggunakan pengkondisian dimana ketika menu tidak null 
    //maka akan ditampilkan Meja dan menu pesanan
    //apabila = null ,maka akan menampilkan meja tak memiliki pesanan
    public void tampilkanPesanan(int nomorMeja) {
        Meja meja = daftarMeja[nomorMeja - 1];
        Pelanggan pelanggan = meja.getPelanggan();
        Menu[] menu = meja.getMenu();
        if (pelanggan != null && menu != null && menu.length > 0) {
            for (int i = 0; i < menu.length; i++) {
                if (menu[i] != null) {
                    System.out.println("Meja " + nomorMeja + " - " + pelanggan.getNama() + " memesan " + menu[i].getNama() + " seharga " + menu[i].getHarga());
                }
            }
        } else {
            System.out.println("Meja " + nomorMeja + " tidak memiliki pesanan");
        }
    }    
// menampilkan daftar menu
    public void tampilkanDaftarMenu() {
        System.out.println("Daftar Menu:");
        System.out.println("1. Nasi Goreng - Rp15.000");
        System.out.println("2. Mi Goreng - Rp15.000");
        System.out.println("3. Capcay - Rp20.000");
        System.out.println("4. Bihun Goreng - Rp17.000");
        System.out.println("5. Ayam Koloke - Rp25.000");
        System.out.println("6. Simpan");
        System.out.println();
    }

    //menampilkan fitur yang dapat diinteraksi kasir
    public void tampilkanDaftarFitur() {
        System.out.println("1. Tampilkan daftar meja");
        System.out.println("2. Tambah pelanggan");
        System.out.println("3. Tambah pesanan");
        System.out.println("4. Hapus pelanggan");
        System.out.println("5. Hitung harga pesanan");
        System.out.println("6. Tampilkan pesanan di meja");
        System.out.println("0. Keluar");
    }

    public void jalankan() {
        Scanner scanner = new Scanner(System.in);
        int pilihan = -1;
        while (pilihan != 0) {
            tampilkanDaftarFitur();
            System.out.print("Masukkan pilihan: ");
            pilihan = scanner.nextInt();
            scanner.nextLine();  
            switch (pilihan) {
                case 1:
                    // menampilkan daftar meja dengan method yang sudah ada
                    //EDIT DISINI
                    tampilkanDaftarMeja();
                    break;
                case 2:
                    // tampilkan pesan untuk input nomor meja dan nama pelanggan untuk digunakan pada method
                    // jangan lupa instansiasi Pelanggan dengan nama pelanggan sesuai input
                    // EDIT DISINI
                    //meminta untuk mengiinput nama dan nomor meja 
                    //pada method tambahPelanggan()
                    System.out.print("nomor meja: ");
                    int nomorMeja = scanner.nextInt();
                    scanner.nextLine();
                    System.out.print("nama pelanggan : ");
                    String namaPelanggan = scanner.nextLine();
                    Pelanggan p = new Pelanggan(namaPelanggan);
                    tambahPelanggan(nomorMeja, p);
                    break;

                case 3:
                boolean stopLoop = false;
                System.out.print("Masukkan nomor meja: ");
                int nomorMejaPesan = scanner.nextInt();
                Boolean meja = daftarMeja[nomorMejaPesan - 1].isKosong();
                scanner.nextLine();  
                if (!meja) {
                    tampilkanDaftarMenu();
                    while (!stopLoop) {
                        System.out.print("Masukkan nomor menu: ");
                        int nomorMenuPesan = scanner.nextInt();
                        scanner.nextLine();  
                        switch (nomorMenuPesan) {
                            case 1:
                                tambahPesanan(nomorMejaPesan, daftarMenu[0]);
                                break;
                            case 2:
                                tambahPesanan(nomorMejaPesan, daftarMenu[1]);
                                break;
                            case 3:
                                tambahPesanan(nomorMejaPesan, daftarMenu[2]);
                                break;
                            case 4:
                                tambahPesanan(nomorMejaPesan, daftarMenu[3]);
                                break;
                            case 5:
                                tambahPesanan(nomorMejaPesan, daftarMenu[4]);
                                break;
                            case 6:
                                stopLoop = true;
                                break;
                            default:
                                System.out.println("Nomor menu tidak valid");
                                break;
                        }
                    }
                }
                else {
                    System.out.println("Meja tidak ada pelanggan");
                }
                break;
                case 4:
                    // untuk menghapus pelanggan pada meja tertentu
                    // tampilkan pesan untuk memasukkan nomor meja yang akan dihapus untuk digunakan pada method hapusPelanggan()
                    // EDIT DISINI
                    //nomor yang dimasukkan akan mereset data meja menjadi kosong atau null
                    System.out.print("nomor meja: ");
                    int nomormeja = scanner.nextInt();
                    hapusPelanggan(nomormeja);
                    break;
                case 5:
                    // Untuk melihat total harga pesanan pada meja tertentu
                    // tampilkan pesan untuk memasukkan nomor meja 
                    // jangan lupa membedakan keluaran apabila pelanggan belum memesan apapun / total harga 0
                    // EDIT DISINI
                    //menampilkan pesanan dengan pengkondisian 
                    //jika total harga = nol,dipastikan tak ada pelanggan pada meja
                    //selain itu pasti ada pelanggan
                    System.out.print("no meja: ");
                    int noMeja = scanner.nextInt();
                    int totalH = hitungHargaPesanan(noMeja);
                    if (totalH==0) {
                        System.out.println("Meja tidak memilikki pesanan");
                    } else {
                        System.out.println("harga pesanan pada meja no "+noMeja+" adalah "+totalH);
                    }
                    break;
                    
                case 6:
                    // untuk melihat pesanan pada meja tertentu
                    // tampilkan pesan untuk memasukkan nomor meja 
                    // EDIT DISINI
                    System.out.print("no meja: ");
                    int NomoRmeja = scanner.nextInt();
                    tampilkanPesanan(NomoRmeja);
                    break;

                case 0:
                    System.out.println("Terima kasih telah menggunakan aplikasi kasir restoran!");
                    break;
                default:
                System.out.println("Pilihan tidak valid");
                break;
        }
        System.out.println();
    }
    scanner.close();
    }
}